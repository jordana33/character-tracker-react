import React, { Component } from 'react';
import Button from './Button';
import './PageControls.css';

class PageControls extends Component {

	handleBtnKeyPress(event) {
	  // Check to see if space or enter were pressed
	  if (event.key === 'Enter' || event.key === ' '){
	    // Prevent the default action to stop scrolling when space is pressed
	    event.preventDefault();
	    this.props.handleClick(event);
	  }
	}

	createHideShowLabel(){
		return this.props.defaultHideShow === "hide" ? "Hide All" : "Show All";
	}

	render() {
		return(
			<section className="button-container">
				<Button label="Short Rest" handleClick= {this.props.onShortRestClick} />
				<Button label="Long Rest" handleClick= {this.props.onLongRestClick} />
				<Button label={this.createHideShowLabel()} handleClick= {this.props.onToggleAllClick} />
			</section>
		);
	}
}

export default PageControls;