import React, { Component } from 'react';
import './Button.css';

class Button extends Component {
	constructor(props) {
	  super(props);
	  this.handleBtnKeyPress = this.handleBtnKeyPress.bind(this);
	}

	handleBtnKeyPress(event) {
	  // Check to see if space or enter were pressed
	  if (event.key === 'Enter' || event.key === ' '){
	    // Prevent the default action to stop scrolling when space is pressed
	    event.preventDefault();
	    this.props.handleClick(event);
	  }
	}

	render() {
		return(<span className="btn" tabIndex="0" onClick={this.props.handleClick} onKeyPress={this.handleBtnKeyPress}>{this.props.label}</span>);
	}
}

export default Button;