import React, { Component } from 'react';
import './PageHeader.css';

// todo - I am passing name around as prop from 2 levels up. feels akward.
class PageHeader extends Component {
	render() {
		return (
    <header>
      <h1>{this.props.name}</h1>
      <div>{this.props.description}</div>
    </header>
    );
  }
}

export default PageHeader;