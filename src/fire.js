import firebase from 'firebase'

// Initialize Firebase
var config = {
  apiKey: "AIzaSyARtz4VJwmTGJOzxSXTMU14kxZZfo8Zglc",
  authDomain: "character-tracker.firebaseapp.com",
  databaseURL: "https://character-tracker.firebaseio.com",
  projectId: "character-tracker",
  storageBucket: "character-tracker.appspot.com",
  messagingSenderId: "183751405524"
};
var fire = firebase.initializeApp(config);

export default fire;