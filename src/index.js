import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import fire from './fire';
import AppError from './AppError';
import registerServiceWorker from './registerServiceWorker';
import './index.css';
import  {arrayToStorage} from './helpers';

// const shortRestListElName = "SHORT-REST-POWER-LIST";
// const shortRestSlotsElName = "SPELL-SLOT-LIST";
// const longRestListElName = "LONG-REST-POWER-LIST";
// const atWillListElName = "AT-WILL-POWER-LIST";
var character = [];
// todo mulitple issues in IE11
// todo hosting this on myjson at the moment to avoid CORS issue.  fix when get this hosted

function loadCharacter() {
  if(!localStorage.getItem('R-currentHp')) {
    fire.database().ref().once('value')
        .then( data => {
          character = data.val();
          console.log('character');
        })
        .then(() => {
            // set data from ajax request to localstorage
            localStorage.setItem('R-name', character.name);
            localStorage.setItem('R-description', character.description);
            localStorage.setItem('R-currentHp', character.hitPoints.maxHp);
            localStorage.setItem('R-maxHp', character.hitPoints.maxHp);
            localStorage.setItem('R-availableSpellSlots',  character.shortRest.spellSlots.available);
            localStorage.setItem('R-hpLog', '[]');
            localStorage.setItem('R-usedShortRestPowers', '[]');
            localStorage.setItem('R-usedLongRestPowers', '[]');
            arrayToStorage('R-atWill', character.atWill.powers);
            arrayToStorage('R-spellSlots', character.shortRest.spellSlots.spells);
            arrayToStorage('R-shortRestPowers', character.shortRest.powers);
            arrayToStorage('R-longRestPowers', character.longRest.powers);
            renderPage();
        })
        .catch(error => {console.log('error is', error)
            ReactDOM.render(<AppError />, document.getElementById('root'));
        });
    } else {
        renderPage();
    }
  } 

function renderPage(){
    ReactDOM.render(<App name= {character.name} description={character.description}/>, document.getElementById('root'));
    registerServiceWorker();
}

 (function init(){loadCharacter();})();