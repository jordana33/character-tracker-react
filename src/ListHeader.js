import React, {Component} from 'react';

class ListHeader extends Component{

	render(){
		return(
			<h2>{this.props.label} <span className='vis-toggle' onClick={this.props.onHideToggle}>({this.props.toggleState})</span></h2>
		);
	}
}

export default ListHeader;