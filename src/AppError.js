import React, { Component } from 'react';

class AppError extends Component{
	render(){
		return (<h1>Sorry there was an Error loading data</h1>);
	}
}

export default AppError;