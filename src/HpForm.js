import React, { Component } from 'react';
import Button from './Button';
import ListHeader from './ListHeader';
import './HpForm.css';

class HpForm extends Component {

	constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
    	hpChangeType: 'damage',
      amount: '',
      notes: '',
    	toggle: this.props.defaultHideShow
    };
  }

  componentWillReceiveProps(nextProps){
    this.setState({toggle: nextProps.defaultHideShow,
                   currentHp: nextProps.currentHp})
  }

  handleSubmit(event) {
  	this.props.onModifyHp(this.state.hpChangeType, this.state.amount, this.state.notes);
  }

  handleChange(event) {
   	let newState = {};
   	newState[event.target.name] = event.target.value;
   	this.setState(newState);
  }

  // only show log if it has data
  checkForHideLog() {
    return this.props.hpLog.length  === 0 ? 'hidden' : '';
  }

  render() {
    return(
      <section>
				<span className="critical-info-label"> Current HP : {this.props.currentHp}</span> <span class="current-hp critical-info"></span>
				<form className="hit-point-form">
					<input type="radio" name="hpChangeType" value="damage" checked={this.state.hpChangeType === 'damage'} onChange={this.handleChange} />damage
					<input type="radio" name="hpChangeType" value="healing" checked={this.state.hpChangeType === 'healing'} onChange={this.handleChange}/>healing 
					<input type="number" name = "amount" placeholder = "amount" value={this.state.amount} onChange={this.handleChange} />
					<input type="text"  name = "notes" placeholder= "notes" value={this.state.notes} onChange= {this.handleChange}/>
					<Button label="submit" handleClick= {this.handleSubmit} />
				</form>
				<div className = "full-hit-points">Maxiumum Hit Points: {this.props.maxHp}</div>
				<div className = { this.checkForHideLog() }>
  			   <ListHeader label="Hit Points" onHideToggle={this.props.onHideToggle.bind(this)} toggleState={this.state.toggle}/>
  				 <ul className={`${this.state.toggle}`}>
            {this.props.hpLog.map(function(logItem) {
              return <li> {logItem.type}: {logItem.amount} at {logItem.date}.  Current HP: {logItem.currentHp}  {logItem.notes}</li>;
            })}
          </ul>
				</div>

			</section>
			);
		}
	}

export default HpForm;