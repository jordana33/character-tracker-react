// Array in and out of local storage
export function arrayToStorage(arrayName, arrayToStore){
  let JSONReadyArray = JSON.stringify(arrayToStore);
  localStorage.setItem(arrayName, JSONReadyArray);
 }

export function arrayFromStorage(arrayName){
  let JSONArray = localStorage.getItem(arrayName);
	let retrievedArray = JSON.parse(JSONArray);
  return retrievedArray;
}
