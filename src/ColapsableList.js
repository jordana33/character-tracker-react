import React, {Component} from 'react';
import ListHeader from './ListHeader';
import './ColapsableList.css';

class ColapsableList extends Component{
	constructor(props) {
    super(props);
    this.state = {
    	toggle: this.props.defaultHideShow
    };
  }

  componentWillReceiveProps(nextProps){
    this.setState({toggle: nextProps.defaultHideShow})
  }

  // Spell slots section needs spell slot summary
  checkForSpellSlots(){
    if (this.props.availableSpellSlots){
      return(<span><span className='critical-info-label'>Available Slots </span><span className='critical-info'>{this.props.availableSpellSlots}</span></span>);
    }
  }

  // If list items need a button, build it
  checkForButton(idPrefix, itemId){
  	if(this.props.buttonText){
  		let idText = idPrefix + "-" + itemId;
  		let buttonClasses = "btn";
  		let buttonText = this.props.buttonText;
  		// disable buttons for used powers and spell slots
  		if (this.props.usedPowers){
  			if (this.props.usedPowers.includes(itemId)){
  				buttonClasses = buttonClasses + " disabled";
  			  buttonText = 'Need Rest';
  			}
  		} else if (this.props.availableSpellSlots < 1){
  			buttonClasses = buttonClasses + " disabled";
  			buttonText = 'Need Rest';
  		}
  		return (<span className={buttonClasses} id={idText} onClick={() => this.props.onButtonClick(itemId, idPrefix)}>{buttonText}</span>);
  	}
  }

	render(){
		return(
  		<section>
  			<ListHeader 
          label={this.props.label} 
          onHideToggle={this.props.onHideToggle.bind(this)} 
          toggleState={this.state.toggle}
        />

        {this.checkForSpellSlots()}
        
				<ul className={`${this.state.toggle}`}>
          {this.props.list.map(function(listItem) {
            let idPrefix = this.props.label.replace(/\s+/g, '');
            return(
            	<li>
              	{this.checkForButton(idPrefix, listItem.id)}
                <div>
                	<span className="title">{listItem.name}</span> 
                  &#160;
                	<span className="notes">{listItem.notes}</span>
                	<span className="type">{listItem.type}</span>
                </div>
            	</li>
            );
          }, this)}
        </ul>
  		</section>
		);
	}
}

export default ColapsableList