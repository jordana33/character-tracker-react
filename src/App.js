import React, { Component } from 'react';
import './App.css';
import PageHeader from './PageHeader';
import PageControls from './PageControls';
import HpForm from './HpForm';
import ColapsableList from './ColapsableList';
import  {arrayFromStorage}  from './helpers';
import  {arrayToStorage}  from './helpers';

class App extends Component {
	constructor(props) {
    super(props);
    this.handleLongRest = this.handleLongRest.bind(this);
    this.handleShortRest = this.handleShortRest.bind(this);
    this.handleHideToggleAll = this.handleHideToggleAll.bind(this);
    this.handlePowerUsed = this.handlePowerUsed.bind(this);
    this.handleSpellSlotsUsed = this.handleSpellSlotsUsed.bind(this);
    this.handleModifyHp = this.handleModifyHp.bind(this);
    this.state = {
      name: localStorage.getItem('R-name'),
      description: localStorage.getItem('R-description'),
      defaultHideShow: 'hide',
      currentHp: parseInt(localStorage.getItem('R-currentHp')),
      maxHp: parseInt(localStorage.getItem('R-maxHp')),
      hpLog: arrayFromStorage('R-hpLog'),
    	atWillPowers: arrayFromStorage('R-atWill'),
      longRestPowers: arrayFromStorage('R-longRestPowers'),
      usedLongRestPowers: arrayFromStorage('R-usedLongRestPowers'),
      shortRestPowers: arrayFromStorage('R-shortRestPowers'),
      usedShortRestPowers: arrayFromStorage('R-usedShortRestPowers'),
      spellSlots: arrayFromStorage('R-spellSlots'),
      availableSpellSlots: parseInt(localStorage.getItem('R-availableSpellSlots'))
    };
  }

  // hide or show a single section
	handleHideToggle(event) {
	 	if (this.state.toggle === 'hide'){
	 		this.setState({toggle: 'show'});
	 	} else {
	 		this.setState({toggle: 'hide'});
	 	}
	}

  // hide or show all sections
  handleHideToggleAll(event) {
    if (this.state.defaultHideShow === 'hide'){
      this.setState({defaultHideShow:'show'});
    } else {
      this.setState({defaultHideShow:'hide'});
    }
  }

  // reset spell slots and Short Rest Powers
  handleShortRest(){
    this.setState({availableSpellSlots: 4,
                    usedShortRestPowers: []
                  });
  }

  //reset short rest powers plus long rest and hit points
  handleLongRest(){
    this.handleShortRest();
    this.setState({usedLongRestPowers: [],
                    currentHp: this.state.maxHp,
                    hpLog: []
                  })
  }

  // use long/short rest power button handler for collapsable list component
  handlePowerUsed(id, powerType) {
    //point to long or short rest used powers array
    let usedPowers = this.state['used'+powerType];
    // if power has already been used ignore click and return
    // todo IE11 doesn't support includes...look for polyfill
    if (usedPowers.includes(id)){
      console.log('already disabled')
      return;
    } else {
    // otherwise add to the list of used powers and re-render buttons
      usedPowers.push(id);
      this.setState({['used'+powerType]:usedPowers});
    }
  }
  
  // spell slot handler for colapsable list component
  handleSpellSlotsUsed() {
    //decrement spell slots if available number is greater than 0 and re-render
    if (this.state.availableSpellSlots < 1){
      console.log('Available Spells already 0');
      return;
    } else {
      let tempSlots = this.state.availableSpellSlots;
      tempSlots--;
      this.setState({availableSpellSlots: tempSlots});
    }
  }

  // modifty hit point handler for hpform component
  handleModifyHp(hpChangeType, amount, notes) {
    // todo .. make control for temporary hitpoints 
    // todo .. review D&D rules for unconsious/death..add those states to sheet?  Disable spells when neg hp?

    //check if damage or healing and change hit points accordingly
    let newHp  = (hpChangeType === 'damage'? this.state.currentHp - parseInt(amount) : this.state.currentHp  + parseInt(amount));
    let newHpLog = this.state.hpLog;
    let d = new Date();
    let hpChangeRecorded = d.toLocaleTimeString([],{hour: '2-digit', minute: '2-digit'}) + ' on ' + d.toLocaleDateString();

    if (newHp > this.state.maxHp) { /* don't go over maxHP */
      newHp = this.state.maxHp;
    };
    // update state and localstorage with new values
    localStorage.setItem('R-currentHp', newHp);
    newHpLog.push({type:hpChangeType, amount:amount, notes:notes, date:hpChangeRecorded, currentHp:newHp});
    this.setState({
      currentHp: newHp,
      hpLog: newHpLog
    })
    arrayToStorage('R-hpLog', this.state.hpLog);
  }
  
  render() {
  return(
  	<main>
    	<PageHeader 
        name= {this.state.name} 
        description={this.state.description}
      />

      {/* Page Level Control Buttons */ }
      <PageControls 
        onToggleAllClick = {this.handleHideToggleAll} 
        onShortRestClick = {this.handleShortRest} 
        onLongRestClick = {this.handleLongRest} 
        defaultHideShow = {this.state.defaultHideShow} 
        onToggleAllClick = {this.handleHideToggleAll}
      />
      
      {/* Hit Point form and log */}
    	<HpForm 
        currentHp = {this.state.currentHp} 
        maxHp = {this.state.maxHp}
        hpLog = {this.state.hpLog} 
        onModifyHp = {this.handleModifyHp}
        defaultHideShow = {this.state.defaultHideShow} 
        onHideToggle = {this.handleHideToggle}
      />

    	{/* At Will Powers */}
      <ColapsableList 
        label="At Will Powers" 
        list={this.state.atWillPowers} 
        defaultHideShow = {this.state.defaultHideShow} 
        onHideToggle = {this.handleHideToggle} 
        buttonText = "" 
      />

      {/* Spell Slots */}
      <ColapsableList 
        label="Spell Slots" 
        list={this.state.spellSlots} 
        availableSpellSlots = {this.state.availableSpellSlots} 
        defaultHideShow = {this.state.defaultHideShow}
        onHideToggle = {this.handleHideToggle} 
        onButtonClick = {this.handleSpellSlotsUsed} 
        buttonText = "Cast Spell"
      />

      {/* Short Rest Powers */}
      <ColapsableList 
        label="Short Rest Powers" 
        list={this.state.shortRestPowers} 
        usedPowers = {this.state.usedShortRestPowers} 
        defaultHideShow = {this.state.defaultHideShow} 
        onHideToggle = {this.handleHideToggle} 
        onButtonClick = {this.handlePowerUsed} 
        buttonText = "Use Power"
      />

      {/* Long Rest Powers */}
      <ColapsableList 
        label="Long Rest Powers" 
        list={this.state.longRestPowers} 
        usedPowers = {this.state.usedLongRestPowers} 
        defaultHideShow = {this.state.defaultHideShow} 
        onHideToggle = {this.handleHideToggle} 
        onButtonClick = {this.handlePowerUsed} buttonText = "Use Power"
      />
  	</main>
  	);
  }

}

export default App;